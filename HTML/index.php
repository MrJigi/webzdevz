<?php
include "classes/testConfig.class.php";
session_start();
//remove to check logged out
// $uname=1;
// $_SESSION['uname'] = $uname;

$loginStatus=false;
if (isset($_SESSION['uid']) || !empty($_SESSION['uid'])){ 
  
  $userid= $_SESSION['uid'];
 }
 else{
   $userid=0;
 }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css\style.css" />
    <title>Index-Page-LS2</title>

    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
      integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
      crossorigin="anonymous"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Spartan&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Audiowide|Viga|Lacquer&display=swap"
      rel="stylesheet"
    />
    <script type="text/javascript">var userid= parseInt(<?php echo json_encode($userid); ?>) ;
    var logInStatus= <?php echo json_encode($loginStatus); ?>; </script>
    <script src="javascript/QuickSearch.js" defer></script>
    <script src="javascript/nav.js" defer></script>
  </head>
  <body>
    
    <div class="page-wrapper">
      <?php include 'includes\navbar.inc.php'; ?>
      
      <!-- SliderSection -->

      <section class="slider-head">
        <video id="slider" autoplay muted loop>
          <source class="videoProp" src="SliderVideos/Borderlands 3 - Psycho Ad_ Beverage.mp4" type="video/mp4">
        </video>
     
        <ul class="thumbnails">
         <li onclick="videoUrl('SliderVideos/Borderlands 3 - Psycho Ad_ Beverage.mp4')"><img src="img/test1/Borderlands3.png"></li>
          <li onclick="videoUrl('SliderVideos/Destiny 2.mp4')"><img src="img/test1/Destiny2.jpg"></li>
          <li onclick="videoUrl('SliderVideos/GRIS.mp4')"><img src="img/test1/Gris.jpg"></li>
          <li onclick="videoUrl('SliderVideos/Subnautica_ Below Zero.mp4')"><img src="img/test1/Subnautica below zero.jpg"></li>  


        </ul>
      </section>
      <script type="text/javascript">
      function videoUrl(SliderVideos){
          document.getElementById("slider").src = SliderVideos;
      }
      
      </script>

      


      <section class="qsearch-area">
        <div class="sbuttons">
          <span id="qall" class="btn all">
            <p>ALL</p>
          </span>
          <span id="qpc" class="btn pc">
            <i class="fas fa-desktop fa-2x"></i>
          </span>
          <span id="qxbox" class="btn xbox">
            <i class="fab fa-xbox fa-2x"></i>
          </span>
          <span id = "qps"class="btn ps">
            <i class="fab fa-playstation fa-2x"></i>
          </span>
        </div>
      </section>
      
      <section class="features">
        <?php include 'includes\productCards.inc.php';?>
      </section>
    </div>
    
    
    <script>
      /*PREVIOUS JQUERY NavigationBar*/
      // $(".menu-toggle").click(function() {
      //   $(".nav").toggleClass("mobile-nav");
      //   $(this).toggleClass("is-active");
      // });


      // var gamespc = ".game.pc";
      // var gamexbox = ".game.xbox";
      // var gamesps = ".game.ps";


      // var selectbtn = ".btn";
      // $(selectbtn).on("click", function() {
      //   $(selectbtn).removeClass("active");
      //   $(this).addClass("active");

        // $(gamespc).removeClass("none");
        // $(gamespc).addClass("none");
      //});
    </script>
  </body>
</html>
