
/*PREVIOUS VERSION JQUERY */
// $(".menu-toggle").click(function() {
//   $(".nav").toggleClass("mobile-nav");
//   $(this).toggleClass("is-active");
// });
// function isEmpty(value){
//   return (value==null || value ==0);
// }

window.onload = function() {
  if(userid==0 && logInStatus == true){
    document.getElementById("account-nav-btn").style.display = "none";
    document.getElementById("cart-nav-btn").style.display = "none";
    document.getElementById("login-nav-btn").style.display = "none";
    document.getElementById("logout-nav-btn").style.display = "none";
  }
  
  if(userid==0){
    document.getElementById("account-nav-btn").style.display = "none";
    document.getElementById("cart-nav-btn").style.display = "none";
    document.getElementById("login-nav-btn").style.display = "show";
    document.getElementById("logout-nav-btn").style.display = "none";
  }
  
  if(userid>0){
    document.getElementById("account-nav-btn").style.display = "show";
    document.getElementById("cart-nav-btn").style.display = "show";
    document.getElementById("logout-nav-btn").style.display = "show";
    document.getElementById("login-nav-btn").style.display = "none";
  }
  
  //DEFINE VARiABLES
  let toggle = document.querySelector(".menu-toggle");
  let items= document.getElementsByClassName("nav");
  //ADD EVENT LISTENER AND LOOP THRU THE LIST TO TOGGLE EACH ELEMENT
  toggle.addEventListener("click", (e) => { 
    toggle.classList.toggle("is-active");
    for (i = 0; i < items.length; i++) {
      items[i].classList.toggle('mobile-nav');
  }
   });
  
  
};
