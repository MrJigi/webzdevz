// SELECTING ALL INPUT ELEMENTS & ERROR DISPLAY ELEMENTS
var username = document.forms['register-form']['username-register'];
var email = document.forms['register-form']['email-register'];
var password = document.forms['register-form']['pass-register'];
var name_error = document.getElementById('name_error');
var email_error = document.getElementById('email_error');
var password_error = document.getElementById('password_error');
//var db_name_error = document.getElementById('php-error');

// SETTING ALL EVENT LISTENERS
username.addEventListener('blur', nameVerify, true);
email.addEventListener('blur', emailVerify, true);
password.addEventListener('blur', passwordVerify, true);

//VALIDATE FUNCTION
function ValidateRegister() {
  // validate username
 // if(db_name_error.value != ""){
 //   db_name_error.style.color="red";
 //   db_name_error.textContent = "Username is required";
 // }
  if (username.value == "") {
    username.style.border = "1px solid red";
    name_error.style.color="red";
    name_error.textContent = "Username is required";
    username.focus();
    return false;
  }

  if (username.value.lenght <4) {
    username.style.border = "1px solid red";
    name_error.style.color="red";
    name_error.textContent = "Username is to short";
    username.focus();
    return false;
  }
  
  // validate email
  if (email.value == "") {
    email.style.border = "1px solid red";
    email_error.style.color="red";
    email_error.textContent = "Email is required";
    email.focus();
    return false;
  }
  if (email.value.length <5 ) {
    email.style.border = "1px solid red";
    email_error.style.color="red";
    email_error.textContent = "Email is to short";
    email.focus();
    return false;
  }
  // validate password
  if (password.value == "") {
    password.style.border = "1px solid red";
    password.style.border = "1px solid red";
    password_error.style.color="red";
    password_error.textContent = "Password is required";
    password.focus();
    return false;
  }
  if (password.value.length <5) {
    password.style.border = "1px solid red";
    password.style.border = "1px solid red";
    password_error.style.color="red";
    password_error.textContent = "Password is too short";
    password.focus();
    return false;
  }
  //check if password matches the username
  if (password.value == username.value) {
    password.style.border = "1px solid red";
    password.style.border = "1px solid red";
    password_error.style.color="red";
    password_error.textContent = "Password can not be the same as the username";
    password.focus();
    return false;
  }
  
}
// FUNCTIONS FOR THE EVENT HANDLERS
function nameVerify() {
  if (username.value != "") {
   name_error.innerHTML = "";
   return true;
  }
}
function emailVerify() {
  if (email.value != "") {
  	email_error.innerHTML = "";
  	return true;
  }
}
function passwordVerify() {
  if (password.value != "") {
  	password_error.innerHTML = "";
  	return true;
  }
}
//OLD Ajax for referance
// var ajax = new XMLHttpRequest();
//     ajax.open("GET", "ajaxphpuserdata.php", true);
//     ajax.send();
//     ajax.onreadystatechange = function() {
//       if (this.readyState == 4 && this.status == 200) {
//      // console.log(this.responseText);
//       var data = JSON.parse(this.responseText);
//       console.log(data);
//       var html = "";
//       for(var a = 0; a < data.length; a++) {
//       var uusername = data[a].username;
//       var ppass = data[a].pass;
//       var eemail = data[a].email;
//       //html="<p>Username: <br>Password: {PlaceHolder}</p>";
//       html+="<p>Username: ";
//       html+=uusername;
//       html+="<br>Password: ";
//       html+=ppass;
//       html+="<br>Email: ";
//       html+=eemail;
//       html+="<p/>";
      
// }
// document.getElementById("aut").innerHTML += html;
//       }
// };