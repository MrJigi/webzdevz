// SELECTING ALL INPUT ELEMENTS & ERROR DISPLAY ELEMENTS
var username = document.forms['userform']['username'];
var email = document.forms['userform']['uemail'];
var password = document.forms['userform']['upassword'];
var password_confirm = document.forms['userform']['uconfirmpassword'];
var fname = document.forms['userform']['firstName'];
var lname = document.forms['userform']['lastName'];
var name_error = document.getElementById('name_error');
var email_error = document.getElementById('email_error');
var password_error = document.getElementById('password_error');
var fname_error = document.getElementById('fname_error');
var lname_error = document.getElementById('lname_error');


// SETTING ALL EVENT LISTENERS
username.addEventListener('blur', nameVerify, true);
email.addEventListener('blur', emailVerify, true);
password.addEventListener('blur', passwordVerify, true);
fname.addEventListener('blur', fnameVerify, true);
lname.addEventListener('blur', lnameVerify, true);

//VALIDATE FUNCTION
function Validate() {
  // validate username
  if (username.value == "") {
    username.style.border = "1px solid red";
    name_error.style.color="red";
    name_error.textContent = "Username is required";
    username.focus();
    return false;
  }
  // validate username
  if (username.value.length < 3) {
    username.style.border = "1px solid red";
    name_error.style.color="red";
    name_error.textContent = "Username must be at least 3 characters";
    username.focus();
    return false;
  }
  // validate name
  if(fname.value == ""){
    fname.style.border = "1px solid red";
    fname_error.style.color="red";
    fname_error.textContent = "Firstname is required";
    fname.focus();
    return false;
  }

  if(lname.value == ""){
    lname.style.border = "1px solid red";
    lname_error.style.color="red";
    lname_error.textContent = "Lastname is required";
    lname.focus();
    return false;
  }

  if(fname.value.match(/^[A-Za-z]+$/)==false){
    fname.style.border = "1px solid red";
    fname_error.style.color="red";
    fname_error.textContent = "Firstname can't contain numbers";
    fname.focus();
    return false;
  }

  if(lname.value.match(/^[A-Za-z]+$/)==false){
    lname.style.border = "1px solid red";
    lname_error.style.color="red";
    lname_error.textContent = "Lastname can't contain numbers";
    lname.focus();
    return false;
  }

  // validate email
  if (email.value == "") {
    email.style.border = "1px solid red";
    email_error.style.color="red";
    email_error.textContent = "Email is required";
    email.focus();
    return false;
  }
  // validate password
  if (password.value == "") {
    password.style.border = "1px solid red";
    password_confirm.style.border = "1px solid red";
    password_error.style.color="red";
    password_error.textContent = "Password is required";
    password.focus();
    return false;
  }
  //check if password matches the username
  if (password.value == username.value) {
    password.style.border = "1px solid red";
    password_confirm.style.border = "1px solid red";
    password_error.style.color="red";
    password_error.textContent = "Password can not be the same as the username";
    password.focus();
    return false;
  }
  // check if the the entered value in password input matches the value in password confirm input
  if (password.value != password_confirm.value) {
    password.style.border = "1px solid red";
    password_confirm.style.border = "1px solid red";
    password_error.style.color="red";
    password_error.innerHTML = "The two passwords do not match";
    return false;
  }

  
}
// FUNCTIONS FOR THE EVENT HANDLERS
function nameVerify() {
  if (username.value != "") {
   name_error.innerHTML = "";
   return true;
  }
}

function emailVerify() {
  if (email.value != "") {
  	email_error.innerHTML = "";
  	return true;
  }
}
function passwordVerify() {
  if (password.value != "") {
  	password_error.innerHTML = "";
  	return true;
  }
  if (password.value == password_confirm.value) {
  	password_error.innerHTML = "";
  	return true;
  }
}

function fnameVerify(){
  if (fname.value != "") {
    fname_error.innerHTML = "";
    return true;
   }
}

function lnameVerify(){
  if (lname.value != "") {
    lname_error.innerHTML = "";
    return true;
   }
}
