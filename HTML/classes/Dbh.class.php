<?php
class Dbh
 {

private $con;
private $host = "localhost"; /* guess */
private $user = "root"; /* User */
private $password = ""; /* Password */
private $dbname = "webzdevz"; /* Database name */


 protected function connect()
  {
 $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
 $pdo = new PDO($dsn, $this->user, $this->password);
 $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
 return $pdo;
  }
}
?>