<?php

class Product
{
    public $id;
    public $title;
    public $description;
    public $platform;
    public $price;
    public $main_image_url;

    function __construct($title, $description, $platform, $price, $main_image_url)
    {
        $this->id=0;
        $this->title=$title;
        $this->description=$description;
        $this->platform=$platform;
        $this->price=$price;
        $this->main_image_url=$main_image_url;
    }

    public function get_id()
    {
        return $this->id;
    }

    public function set_id($value)
    {
        $this->id = $value;
    }

    public function get_title()
    {
        return $this->title;
    }

    public function set_title($value)
    {
        $this->title = $value;
    }

    public function get_description()
    {
        return $this->description;
    }

    public function set_description($value)
    {
        $this->description = $value;
    }

    public function get_platform()
    {
        return $this->platform;
    }

    public function set_platform($value)
    {
        $this->platform=$value;
    }

    public function get_price()
    {
        return $this->price;
    }

    public function set_price($value)
    {
        $this->price=$value;
    }

    public function get_main_image_url()
    {
        return $this->main_image_url;
    }

    public function set_main_image_url($value)
    {
        $this->main_image_url=$value;
    }

}
?>