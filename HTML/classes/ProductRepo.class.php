<?php

class ProductRepo extends Dbh implements IProductRepo
{
    public function getProducts(){
        $sql ="SELECT * FROM products";
        $stmt= $this->connect()->query($sql);
        $productListDb = array();
        while($row= $stmt->fetch())
        {
            $objProduct= new Product($row['title'], $row['description'], $row['platform'], $row['price'], $row['main_image_url']);
            $objProduct->set_id($row['id']);
            
            array_push($productListDb, $objProduct);
        }
        return $productListDb;
    }
}
?>