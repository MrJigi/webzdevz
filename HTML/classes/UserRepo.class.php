<?php

class UserRepo extends Dbh implements IUserRepo
{
   public function add(User $user)
    {
        
        $sql = "INSERT INTO users (username, email, pass) VALUES (?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt -> execute([$user->get_username(),$user->get_email(),$user->get_password()]);
        
    }

    public function getUsers(){
        $sql ="SELECT * FROM users";
        $stmt= $this->connect()->query($sql);
        $userListDb = array();
        while($row= $stmt->fetch())
        {
            $objUser= new User($row['username'], $row['pass']);
            $objUser->set_id($row['id']);
            $objUser->set_email($row['email']);
            $objUser->set_firstName($row['firstName']);
            $objUser->set_lastName($row['lastName']);
            $objUser->set_birthDate($row['birthDate']);
            $objUser->set_gender($row['gender']);
            $objUser->set_credentials($row['credentials']);
            array_push($userListDb, $objUser);
        }
        return $userListDb;
        // $results-> $stmt->fetchAll();
        // return $results;
    }

    public function update(User $user)
    {
        
        $sql = "UPDATE users SET username=?, email=?, pass=?, firstName=?, lastName=?, birthDate=?, gender=? WHERE id=?";
        $stmt = $this->connect()->prepare($sql);
        $stmt -> execute([$user->get_username(),$user->get_email(),$user->get_password(),$user->get_firstName(),$user->get_lastName(),$user->get_birthDate(),$user->get_gender(), $user->get_id()]);
        
    }
}
?>