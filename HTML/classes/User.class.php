<?php

class User
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $firstName;
    public $lastName;
    public $birthDate;
    public $gender;
    public $credentials;

    function __construct($uname, $upassword)
    {
        $this->id=0;
        $this->username=$uname;
        $this->password=$upassword;
        $this->email="";
        $this->firstName="";
        $this->lastName="";
        $this->birthDate="";
        $this->gender="";
        $this->credentials="client";
    }

    public function get_id()
    {
        return $this->id;
    }

    public function set_id($value)
    {
        $this->id = $value;
    }

    public function get_username()
    {
        return $this->username;
    }

    public function set_username($value)
    {
        $this->username = $value;
    }

    public function get_email()
    {
        return $this->email;
    }

    public function set_email($value)
    {
        $this->email = $value;
    }

    public function get_password()
    {
        return $this->password;
    }

    public function set_password($value)
    {
        $this->password=$value;
    }

    public function get_firstName()
    {
        return $this->firstName;
    }

    public function set_firstName($value)
    {
        $this->firstName=$value;
    }

    public function get_lastName()
    {
        return $this->lastName;
    }

    public function set_lastname($value)
    {
        $this->lastName=$value;
    }

    public function get_birthDate()
    {
        return $this->birthDate;
    }

    public function set_birthDate($value)
    {
        $this->birthDate = $value;
    }

    public function get_gender()
    {
        return $this->gender;
    }

    public function set_gender($value)
    {
        $this->gender = $value;
    }

    public function get_credentials()
    {
        return $this->credentials;
    }

    public function set_credentials($value)
    {
        $this->credentials = $value;
    }

}
?>