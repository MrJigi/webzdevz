<?php 
interface IUserRepo
{
    function add(User $user);
    function getUsers();
    function update(User $user);
}
?>