<?php
//include 'includes/autoloader.inc.php';

session_start();
//Check if user is logged in
// $uid=1;
//Value pre-set for testing, will set on user-login
// $_SESSION['uid'] = $uid;

$loginStatus=false;
if (isset($_SESSION['uid']) || !empty($_SESSION['uid'])){ 
  
  $userid= $_SESSION['uid'];
 }
 else{
   $userid=0;
 }
 include 'includes/usereditscript.inc.php';
 $Viewer= new UserViewer();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css" />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
      integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
      crossorigin="anonymous"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Spartan&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Audiowide|Viga|Lacquer&display=swap"
      rel="stylesheet"
    />
    <script type="text/javascript">var userid = <?php echo json_encode($userid) ?>;
    var logInStatus= <?php echo json_encode($loginStatus) ?></script>
    <script src="javascript/nav.js" defer></script>
   <script src="javascript/validateuseraccinfo.js" defer></script>
    <title>User-Page</title>
</head>
<body>
    <div class="page-wrapper">
    <?php include 'includes\navbar.inc.php'; ?>
        <section>
          <div class="container-user-info">
            <div class="cover-photo-user">
              <img src="https://image.flaticon.com/icons/svg/17/17004.svg" class="profile-image">
            </div>
            <div id="pid" class="profile-name-user"><p>User-ID: <?php echo $_SESSION['uid'];?></p></div>
            <div id ="aut"class="about-user-text">
            <?php $Viewer->DisplayUser($userid) ?>
            </div>
                   
<div id="uform">
            <form class="form-user" action="" method="post" name="userform" onsubmit="return Validate()">
              <label for="userid" class ="tbUserEdit">Username</label>
              <input class="form-styling-user" type="text" name="username" placeholder=""/>
              <div id="name_error"></div>
              <label for="email" class ="tbUserEdit">Email</label>
              <input class="form-styling-user" type="text" name="uemail" placeholder=""/>
              <div id="email_error"></div>
              <label for="password" class ="tbUserEdit">Password</label>
              <input class="form-styling-user" type="text" name="upassword" placeholder=""/>
              <label for="confirmpassword" class ="tbUserEdit">Confirm password</label>
              <input class="form-styling-user" type="text" name="uconfirmpassword" placeholder=""/>
              <div id="password_error"></div>
              <label for="firstName" class ="tbUserEdit">First Name</label>
              <input class="form-styling-user" type="text" name="firstName" placeholder=""/>
              <div id="fname_error"></div>
              <label for="lastName" class ="tbUserEdit">Last Name</label>
              <input class="form-styling-user" type="text" name="lastName" placeholder=""/>
              <div id="lname_error"></div>
              <label for="gender" class ="tbUserEdit">Gender</label>
              <label class="rbUserEdit"><p class= "rbText">Male</p>
              <input type="radio" name="gender" value="male" checked="checked"/>
              <span class="checkmark"></span>
              </label>
              <label class="rbUserEdit"><p class= "rbText">Female</p>
              <input  type="radio" name="gender" value="female"/>
              <span class="checkmark"></span>
              </label>
              <label class="rbUserEdit"><p class= "rbText">Other</p>
              <input type="radio" name="gender" value="other"/>
              <span class="checkmark"></span>
              </label>
              <label for="birthDate" class ="tbUserEdit">BirthDate</label>
              <input class="form-styling-user" type="date" name="birthdate" value="<?php echo date('Y-m-d'); ?>"/>
              <input type="submit" value="Submit" name="btn-submit-uchange" id="save-user-info-btn" />
              
                    </form>
                  </div>
            <button id="uebtn" class="change-user-info-btn">Change Info</button>
            
          </div>
        </section>
        <script>

          let btnedit = document.getElementById("uebtn");
          let textid = document.getElementById("pid");
          let textau = document.getElementById("aut");

          let uform = document.getElementById("uform");
          

          btnedit.addEventListener("click", (e) => { 
    btnedit.classList.add("hide");
    textid.classList.add("hide");
    textau.classList.add("hide");
    uform.classList.add("show");
   });
   
        </script>
</body>
</html>