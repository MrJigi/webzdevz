

<div class="nav-wrapper">
        <nav class="navbar">
          <img
            src="img/Logotemp.svg"
            alt="Group Logo"
            onclick="javascript:location.href='index.php'"
          />
          <div class="menu-toggle" id="mobile-menu">
            <span class="bar"></span>
            <span class="bar"></span>
            <span class="bar"></span>
          </div>
          <ul class="nav">
            <li id="account-nav-btn" class="nav-item"><a href="userpage.php">Account Settings</a></li>
            <li id="cart-nav-btn" class="nav-item"><a href="#">Cart</a></li>
            <li id="login-nav-btn" class="nav-item"><a href="logIn.php" >Login</a></li>
            <li id="logout-nav-btn" class="nav-item"><a href="logout.php" >Logout</a></li>
          </ul>
        </nav>

      </div>
   