<?php
include '../includes/autoloader.inc.php';
//init variables
$controllerObj = new UserController();
$rerrors = array();

//REGISTER

//get all input values
$rusername=$_POST['username-register'];
$remail=$_POST['email-register'];
$rpassword=$_POST['pass-register'];

/*VALIDATION*/
$validateDb=$controllerObj->validateUsername($rusername);
//validate input data and push them inside array
if (empty($rusername)) { array_push($rerrors, "Username is required"); }
if (empty($remail)) { array_push($rerrors, "Email is required"); }
if (empty($rpassword)) { array_push($rerrors, "Password is required"); }
if($rusername==$rpassword){array_push($rerrors,"Username is the same as password");}
if($validateDb==false){array_push($rerrors,"Username allready exists");}

    //register user if there are no errors
if (count($rerrors) == 0) {
    
    $controllerObj->register($rusername,$remail,$rpassword);
    session_start();
    
    $_SESSION['uid'] = $controllerObj->getId($rusername,$rpassword);
   // echo json_encode($controllerObj->getId($rusername,$rpassword));
   // $_SESSION['register-error']= "";
    header('location: ../index.php');
}
else
{
    
    //$_SESSION['register-error']= "invalid-username";
    //echo  $_SESSION['register-error'];
    header('location: ../logIn.php');
    //echo json_encode($rerrors);
}
//echo json_encode($validateDb);
